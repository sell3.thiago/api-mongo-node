const express = require("express");

// importamos el schema de usuario
const restaurantSchema = require('../models/Restaurant');
const router = express.Router();

// obtener todos los usuarios
router.get('/restaurants', (req, res) => {
    restaurantSchema.find().then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
    
});

// obtener un usuario
router.get('/restaurants/:id', (req, res) => {
    const { id } = req.params;
    restaurantSchema.findById(id).then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
});
// creación de usuario
router.post('/restaurants', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const user = restaurantSchema(req.body);
    user.save().then((data) => {
        res.json({ message: 'Registro creado exitosamente...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});

// actualización de usuario
router.put('/restaurants/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;
    const {  } = req.body;


    restaurantSchema.updateOne({ _id: id }, { $set: {   } })
    .then((data) => {
        res.json({ message: 'Se ha actualizado el usuario satisfactoriamente...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});

// actualización de usuario
router.delete('/restaurants/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;

    restaurantSchema.deleteOne({ _id: id })
    .then((data) => {
        res.json({ message: 'Se ha eliminado el usuario ...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});


module.exports = router;