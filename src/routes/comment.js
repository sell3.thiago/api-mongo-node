const express = require("express");

// importamos el schema de usuario
const commentSchema = require('../models/Comments');
const router = express.Router();

// obtener todos los usuarios
router.get('/comment', (req, res) => {
    commentSchema.find().then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
});

// obtener un usuario
router.get('/comment/:id', (req, res) => {
    const { id } = req.params;
    commentSchema.findById(id).then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
});
// creación de usuario
router.post('/comment', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const user = commentSchema(req.body);
    user.save().then((data) => {
        res.json({ message: 'Registro creado exitosamente...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});

// actualización de usuario
router.put('/comment/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;
    const { user, restaurant } = req.body;

    commentSchema.updateOne({ _id: id }, { $set: { user, restaurant } })
    .then((data) => {
        res.json({ message: 'Registro actualizado exitosamente...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});

// actualización de usuario
router.delete('/comment/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;

    commentSchema.deleteOne({ _id: id })
    .then((data) => {
        res.json({ message: 'Registro eliminado exitosamente... ...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});


module.exports = router;