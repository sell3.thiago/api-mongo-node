const express = require("express");
const jwt = require('jsonwebtoken');

// importamos el schema de usuario
const userSchema = require('../models/User');
const router = express.Router();

// obtener todos los usuarios
router.get('/users', (req, res) => {
    userSchema.find().then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
});

// obtener un usuario
router.get('/users/:id', (req, res) => {
    const { id } = req.params;
    userSchema.findById(id).then((data) => {
        res.json(data);
    }).catch(error => {
        console.log({ message: error });
    });
});
// creación de usuario
router.post('/users', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const user = userSchema(req.body);
    user.save().then((data) => {
        res.json({ message: 'Registro creado exitosamente...', data: data });
    }).catch(error => {
        console.log({ message: error });
    });
});

// actualización de usuario
router.put('/users/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;
    const { name, email, username, password } = req.body;

    userSchema.updateOne({ _id: id }, { $set: { name, email, username, password } })
        .then((data) => {
            res.json({ message: 'Registro actualizado exitosamente...', data: data });
        }).catch(error => {
            console.log({ message: error });
        });
});

// actualización de usuario
router.delete('/users/:id', (req, res) => {
    // pasamos lo que enviamos desde el cuerpo de la peticion al schema
    const { id } = req.params;

    userSchema.deleteOne({ _id: id })
        .then((data) => {
            res.json({ message: 'Registro eliminado exitosamente... ...', data: data });
        }).catch(error => {
            console.log({ message: error });
        });
});


router.post('/auth', async (req, res) => {
    const { username, password } = req.body;

    try {
        const user = await userSchema.findOne({ username });

        if (!user) {
            res.status(401).json({ message: 'Datos incorrectos' });
            return;
        }

        const result = await user.isCorrectPassword(password);

        if (result) {
            const token = jwt.sign({
                name: user.name,
                id: user._id
            }, process.env.TOKEN_SECRET)
            res.header('auth-token', token).json({
                error: false,
                data: {
                    access_token: token, user: user
                }
            })
        } else {
            res.header('auth-token', token).json({
                error: true,
                data: null
            })
        }
    } catch (error) {
        res.header('auth-token', null).json({
            error: true,
            data: error,
        })
    }
});


module.exports = router;