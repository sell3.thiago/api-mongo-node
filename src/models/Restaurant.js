const mongoose = require("mongoose");
const validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};
const restaurantSchema = mongoose.Schema(
    {
        // nombre de restaurante
        restaurant: {
            type: String,
            required: true,
        },
        // Dirección de correo electronico
        email: {
            type: String,
        },
        // Dirección fisica
        address: {
            type: String,
        },
        // instagram
        instagram: {
            type: String,
        },
        // descripcion del restaurante
        description: {
            type: String,
        },
        // Telefono
        phone: {
            type: String,
        },
        // Promedio de calificación 
        rating: {
            type: Number
        },
        urlLogo: {
            type: String,
        },
        typeFood: [String]        
    }, { timestamps: true }
)
module.exports = mongoose.model('Restaurant', restaurantSchema);