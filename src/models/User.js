const mongoose = require("mongoose");
const bcrypt = require("bcrypt")

const saltRounds = 10;
const validateEmail = function (email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};
const userSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            lowercase: true
        },
        username: {
            type: String,
            required: true,
            unique: true,
        },
        password: {
            type: String,
            required: true,
        }
    }, { timestamps: true }
);

userSchema.pre('save', function (next) {
    // validamos que la pass sea nueva o para editar
    if (this.isNew || this.isModified('password')) {
        const document = this;
        // hasheamos la password
        bcrypt.hash(document.password, saltRounds, (err, hashedPassword) => {
            if (err) {
                next(err);
            } else {
                document.password = hashedPassword;
                next();
            }
        })
    } else {
        next();
    }
});

userSchema.methods.isCorrectPassword = function (password) {
    return bcrypt.compare(password, this.password);
};
module.exports = mongoose.model('User', userSchema);