// hacemos uso de express para las rutas de nuestra api
const express  = require('express');
// Mongoose nos permitirá realizar operaciones en mongo
const mongoose = require('mongoose');

const app = express();
// nuestro puerto
const port = process.env.PORT || 3000;
// esta dependencia nos permitirá custoomizar y hacer uso de archivos de entorno
require('dotenv').config();

// cors
var cors = require('cors')

// importamos las rutas
const userRoutes = require('./src/routes/user');
const restaurantRoutes = require('./src/routes/restaurant');
const commentRoutes = require('./src/routes/comment');

const verifyToken = require('./src/routes/validate-token');

// middleware
app.use(express.json());
app.use(cors());
app.use('/api', verifyToken, userRoutes)
app.use('/api', verifyToken, restaurantRoutes)
app.use('/api', verifyToken, commentRoutes)


app.get('/', (req, res) => {
    res.send('Why so serious?')
})

// conexión a la db de mongo de atlas
mongoose.connect(process.env.MONGODB_URI).then(() => {
    console.log("connected to mongodb Atlas");
}).catch((error) => {
    console.log(error);
})

app.listen(port, () => {
    console.log(`Server listening in port ${port}`);
})